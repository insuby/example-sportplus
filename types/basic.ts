import type {Dispatch, SetStateAction} from "react";
import type {FETCH_TYPES} from "@/types/unions";

export type PropertyType<T> = T[keyof T]
export type SetState<T> = Dispatch<SetStateAction<T>>
export type Immutable<T> = { readonly [K in keyof T]: Immutable<T[K]> };
export type OptionsFlags<Type> = { [Property in keyof Type]: boolean };
export type Flatten<T> = T extends any[] ? T[number] : T;
export type Diff<T, U> = T extends U ? never : T;
export type NonEmptyArray<T> = [T, ...T[]]

type NetworkLoadingState = {
    state: FETCH_TYPES & "PENDING";
};
type NetworkFailedState = {
    state: FETCH_TYPES & "FAILED";
    code: number;
};
type NetworkSuccessState = {
    state: FETCH_TYPES & "SUCCESS";
    response: {
        title: string;
        duration: number;
        summary: string;
    };
};

export type NetworkState =
    | NetworkLoadingState
    | NetworkFailedState
    | NetworkSuccessState;

type OneOrAnother<T1, T2> =
    | (T1 & { [K in keyof T2]?: undefined })
    | (T2 & { [K in keyof T1]?: undefined });

