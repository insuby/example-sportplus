import type {ComponentClass, ComponentType, FunctionComponent, RefObject, WeakValidationMap} from "react";
import type {BaseRouter} from "next/dist/shared/lib/router/router";
import type {TABS_TYPES} from "@/types/unions";

export type DynamicComponent =
    | ComponentClass<{} | WeakValidationMap<{}>>
    | FunctionComponent<{} | WeakValidationMap<{}>>

export type HeaderComponent = FunctionComponent<Pick<BaseRouter, 'asPath'>>

export type PopupComponent = ComponentType<{
    popupRef: RefObject<HTMLDivElement>,
    listRef: RefObject<HTMLLIElement>,
    open: boolean,
    time: string,
}>

export type TabsType = Record<NonNullable<Lowercase<TABS_TYPES>>, []>

export type TabsComponent = FunctionComponent<{ title: string, tabs: TabsType }>

export type ExtendProps<T> = T extends ComponentType<infer Props>
    ? Props extends object
        ? Props
        : never
    : never;