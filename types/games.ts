import type {PERIODS_TYPES} from "@/types/unions";

interface GameAllKeys {
    readonly id: number
    readonly participant: string
    readonly startHours: string
    readonly startDate: string
    readonly period: number
    readonly leader: 1 | 0,
    readonly href: string
    readonly participants: [Participant, Participant]
    readonly images: [string, string]
    readonly score: number[]
    readonly periods: number[] & [number[]]
    readonly periodPostfix: PERIODS_TYPES,
    readonly currentTime: number
    readonly activePeriod: number
    readonly coeff: Coeff[] | null
}

type Participant = {
    id: number,
    name: string
}
type Coeff = {
    key: string,
    value: number
}

type GameBasic = NonNullable<Pick<GameAllKeys,
    | 'id'
    | 'href'
    | 'participants'
    | 'images'>>

type GameWithScore = GameBasic & NonNullable<Pick<GameAllKeys,
    | 'score'
    | 'periodPostfix'
    | 'activePeriod'
    | Partial<'periods'>>>

export type GameWithCoeffType = GameBasic & NonNullable<Pick<GameAllKeys,
    | 'coeff'
    | 'startDate'
    | 'startHours'>>

export type GameWithScoreHalfType = GameWithScore & NonNullable<Pick<GameAllKeys, 'currentTime'>>

export type GameWithScoreSetType = GameWithScore
    & Pick<GameAllKeys, 'leader'>
    & {
    periods: [
        [number, number], [number, number],
        [number, number], [number, number],
        [number, number], [number, number],
        [number, number], [number, number],
    ],
}

export type GameWithScoreQuarterType = GameWithScore
    & Pick<GameAllKeys, 'currentTime'>
    & Omit<GameWithScore, 'activePeriod'>
    & {
    periods: [
        number, number, number, number,
        number, number, number, number
    ],
}

const images = [
    'https://cdng.apigodata.com/onexbet/2746897.png?_v=1642759533817',
    'https://cdng.apigodata.com/onexbet/2746897.png?_v=1642759533817'
]

const participants = [{
    id: 2746897,
    name: 'Arsenal'
}, {
    id: 2746897,
    name: 'Arsenal'
}]

export const dummyGameWithCoeff: GameWithCoeffType = {
    id: 123,
    href: '/123123',
    startHours: '15:00',
    startDate: '25.08',
    favorite: false,
    coeff: [{
        key: 'П1', value: 3
    }, {
        key: 'П1', value: 33
    }],
    // @ts-ignore
    images,
    // @ts-ignore
    participants
}

export const dummyGameWithScoreHalf: GameWithScoreHalfType = {
    id: 1212312313,
    href: '123123123123',
    activePeriod: 1,
    currentTime: 19,
    favorite: true,
    periodPostfix: 'HALF',
    score: [2, 1],
    // @ts-ignore
    images,
    // @ts-ignore
    participants
}

export const dummyGameWithScoreSet: GameWithScoreSetType = {
    id: 12223,
    href: '123123123123',
    activePeriod: 1,
    leader: 0,
    periods: [
        // @ts-ignore
        [1, 2],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
        // @ts-ignore
        [1, 3],
    ],
    favorite: true,
    periodPostfix: 'SET',
    score: [2, 1],
    // @ts-ignore
    images,
    // @ts-ignore
    participants
}

export const dummyGameWithScoreQuarter: GameWithScoreQuarterType = {
    id: 3333,
    href: '123123123123',
    activePeriod: 1,
    periods: [
        // @ts-ignore
        134, 121,
        // @ts-ignore
        22, 14,
        // @ts-ignore
        22, 14,
        // @ts-ignore
        22, 14,
        // @ts-ignore
        22, 14
    ],
    currentTime: 19,
    favorite: true,
    periodPostfix: 'QUARTER',
    score: [2, 1],
    // @ts-ignore
    images,
    // @ts-ignore
    participants
}