import {DEFAULT_TIMEZONE} from "@/utils/constants";
import type {ReactNode} from 'react'
import {createContext, useContext, useEffect, useState} from 'react';
import type AppContextProps from "@/types/context";

const AppContext: AppContextProps = createContext({});

const AppContextWrapper: ({children}: { children: ReactNode }) => JSX.Element =
    ({children}) => {
        const [activeTournament, setActiveTournament] = useState({})
        const [activeSport, setActiveSport] = useState({})
        const [activeGame, setActiveGame] = useState('')
        const [activeTab, setActiveTab] = useState(null)
        const [favorite, setFavorite] = useState([])
        const [excludeIds, setExcludeIds] = useState([])
        const [timezone, setTimezone] = useState(null)
        const [time, setTime] = useState('')

        useEffect(() => {
            // @ts-ignore
            const prev = JSON.parse(localStorage.getItem('favorite'))
            prev && setFavorite(prev)
            localStorage.setItem('favorite', JSON.stringify(prev ?? favorite))
            // @ts-ignore
            setTimezone(JSON.parse(localStorage.getItem('timezone')) ?? DEFAULT_TIMEZONE);
        }, [])

        return <AppContext.Provider value={{
            sportContext: {activeSport, setActiveSport},
            tournamentContext: {activeTournament, setActiveTournament},
            gameContext: {activeGame, setActiveGame},
            tabContext: {activeTab, setActiveTab},
            excludeContext: {excludeIds, setExcludeIds},
            favoriteContext: {favorite, setFavorite},
            timezoneContext: {timezone, setTimezone},
            timeContext: {time, setTime},
        }}>
            {children}
        </AppContext.Provider>
    }

export function useAppContext() {
    return useContext(AppContext);
}

export default AppContextWrapper