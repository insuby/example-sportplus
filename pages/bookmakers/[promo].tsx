import Image from "next/image";
import img from '@/public/images/1-win.svg';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {GetStaticProps} from "next";
import type {FunctionComponent} from 'react';

const Bookmakers: FunctionComponent = () => {
    return <main className="page__main bk">
        <section className="bk__block">
            <h1 className="bk__title title-b">Букмекерские конторы России</h1>
            <p>Список только лучших букмекерских контор для ставок России, каждый букмекер как минимум имеет
                одну из двух особенностей:</p>
            <ul>
                <li>большое количество прямых видео трансляций матчей, а значит они идеально подходят для лайв
                    ставок;
                </li>
                <li>высокие коэффициенты (низкая маржа), что значит ставки в этих букмекерских конторах наиболее
                    выгодны.
                </li>
            </ul>
        </section>
        <div className="bk__select select">
            <div className="select__head">Россия</div>
            <div className="select__i">
                <ul className="select__list">
                    <li className="select__item active">Россия</li>
                    <li className="select__item">Украина</li>
                    <li className="select__item">Казахстан</li>
                    <li className="select__item">Беларусь</li>
                    <li className="select__item">СНГ</li>
                </ul>
            </div>
        </div>
        <section className="bk__block">
            <div className="bk__head marathon-bet">
                <a className="bk__image" href="">
                    <Image src={img} alt="img"/>
                </a>
                <div className="bk__info">
                    <span className="text">0% маржа на множество топ матчей</span>
                    <button className="bk__refresh _icon-refresh-cw"/>
                </div>
            </div>
            <div className="bk__margin">
                <div className="bk__card">
                    <div className="bk__card-title _icon-soccer-ball">
                        <span>Футбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-basketball">
                        <span>Баскетбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-tennis-ball">
                        <span>Теннис</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-puck">
                        <span>Хоккей</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
            </div>
            <div className="bk__translation">
                <div className="bk__header _icon-chevron-down">
                    <div className="bk__title-tn title-sm">Трансляции <span className="esm-hide">турниров</span>
                    </div>
                    <div className="bk__score text">
                        <span>Всего 43</span>
                        <span>Сейчас 12</span>
                    </div>
                </div>
                <ul className="bk__list">
                    <li className="bk__item switch">
                        <div className="switch__inner">
                            <div className="switch__item"/>
                            <span>Live</span>
                        </div>
                    </li>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Футбол</div>
                        <div className="bk__amount">6 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Баскетбол</div>
                        <div className="bk__amount">2 турнира</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Теннис</div>
                        <div className="bk__amount">1 турнир</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Хоккей</div>
                        <div className="bk__amount">5 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__text">Трансляции могут быть доступны не для всех матчей, указанных турниров.
                        Возможны региональные ограничения.
                    </li>
                </ul>
            </div>
            <div className="bk__advantage text">Высокие коэффициенты и хороший выбор ставок. На многие ТОП события
                ставки с 0% маржой.
            </div>
        </section>
        <section className="bk__block">
            <div className="bk__head onex-bet">
                <a className="bk__image" href="">
                    <Image src={img} alt="img"/>
                </a>
                <div className="bk__info">
                    <span className="text">0% маржа на множество топ матчей</span>
                    <button className="bk__refresh _icon-refresh-cw"/>
                </div>
            </div>
            <div className="bk__margin">
                <div className="bk__card">
                    <div className="bk__card-title _icon-soccer-ball">
                        <span>Футбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-basketball">
                        <span>Баскетбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-tennis-ball">
                        <span>Теннис</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-puck">
                        <span>Хоккей</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
            </div>
            <div className="bk__translation">
                <div className="bk__header _icon-chevron-down">
                    <div className="bk__title-tn title-sm">Трансляции <span className="esm-hide">турниров</span>
                    </div>
                    <div className="bk__score text">
                        <span>Всего 43</span>
                        <span>Сейчас 12</span>
                    </div>
                </div>
                <ul className="bk__list">
                    <li className="bk__item switch">
                        <div className="switch__inner">
                            <div className="switch__item"/>
                            <span>Live</span>
                        </div>
                    </li>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Футбол</div>
                        <div className="bk__amount">6 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Баскетбол</div>
                        <div className="bk__amount">2 турнира</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Теннис</div>
                        <div className="bk__amount">1 турнир</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Хоккей</div>
                        <div className="bk__amount">5 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__text">Трансляции могут быть доступны не для всех матчей, указанных турниров.
                        Возможны региональные ограничения.
                    </li>
                </ul>
            </div>
            <div className="bk__advantage text">Высокие коэффициенты и хороший выбор ставок. На многие ТОП события
                ставки с 0% маржой.
            </div>
        </section>
        <section className="bk__block">
            <div className="bk__head starz">
                <a className="bk__image" href="">
                    <Image src={img} alt="img"/>
                </a>
                <div className="bk__info">
                    <span className="text">0% маржа на множество топ матчей</span>
                    <button className="bk__refresh _icon-refresh-cw"/>
                </div>
            </div>
            <div className="bk__margin">
                <div className="bk__card">
                    <div className="bk__card-title _icon-soccer-ball">
                        <span>Футбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-basketball">
                        <span>Баскетбол</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-tennis-ball">
                        <span>Теннис</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
                <div className="bk__card">
                    <div className="bk__card-title _icon-puck">
                        <span>Хоккей</span>
                    </div>
                    <div className="bk__card-score">4.0%</div>
                    <div className="bk__card-ratio grey">1.92</div>
                    <div className="bk__card-info _icon-help-circle"/>
                </div>
            </div>
            <div className="bk__translation">
                <div className="bk__header _icon-chevron-down">
                    <div className="bk__title-tn title-sm">Трансляции <span className="esm-hide">турниров</span>
                    </div>
                    <div className="bk__score text">
                        <span>Всего 43</span>
                        <span>Сейчас 12</span>
                    </div>
                </div>
                <ul className="bk__list">
                    <li className="bk__item switch">
                        <div className="switch__inner">
                            <div className="switch__item"/>
                            <span>Live</span>
                        </div>
                    </li>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Футбол</div>
                        <div className="bk__amount">6 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Баскетбол</div>
                        <div className="bk__amount">2 турнира</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Теннис</div>
                        <div className="bk__amount">1 турнир</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__item text _icon-chevron-down">
                        <div className="bk__kind">Хоккей</div>
                        <div className="bk__amount">5 турниров</div>
                    </li>
                    <ul className="bk__sub-list">
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                        <li className="bk__sub-item a">
                            <a className="bk__sub-link active" href="">Чемпионат Испании. Примера</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Испании</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Греции. Суперлига</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Чемпионат Австралии</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link" href="">Кубок Франции</a>
                        </li>
                        <li className="bk__sub-item">
                            <a className="bk__sub-link active" href="">Чемпионат Франции. Вторая лига</a>
                        </li>
                    </ul>
                    <li className="bk__text">Трансляции могут быть доступны не для всех матчей, указанных турниров.
                        Возможны региональные ограничения.
                    </li>
                </ul>
            </div>
            <div className="bk__advantage text">Высокие коэффициенты и хороший выбор ставок. На многие ТОП события
                ставки с 0% маржой.
            </div>
        </section>
    </main>
};

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...await serverSideTranslations(locale, ["categories", "tabs", "common"]),
        },
    }
}

export async function getStaticPaths() {
    return {
        paths: [
            {params: {promo: '/'}},
        ],
        fallback: true // false or 'blocking'
    };
}

export default Bookmakers;