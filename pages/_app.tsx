import '@/styles/index.scss'
import {SWRConfig} from 'swr'
import {logger} from "@/useRequest";
import type {NextRouter} from "next/router";
import {useRouter} from "next/router";
import type {ReactElement} from "react";
import {useEffect} from "react";
import dynamic from 'next/dynamic'
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import AppContextWrapper from "../context";
import {getLanguage} from "@/utils/functions";
import {appWithTranslation} from 'next-i18next'
import type {AppProps} from 'next/app'
import type {DynamicComponent} from "@/types/components";

const AsideMenu: DynamicComponent = dynamic(() => import("@/components/Aside"))
const AsideBanner: DynamicComponent = dynamic(() => import("@/components/Banners/BannerBig"))

const App = (props: AppProps): ReactElement => {
    const {asPath}: NextRouter = useRouter();
    const {Component, pageProps}: AppProps = props;

    useEffect(() => {
        const locale = process.env["NEXT_PUBLIC_DEFAULT_LOCALE"] || getLanguage()
        const [originStart, originEnd] = location.href.split('://')

        if (!location.hostname.match((/^[ru|en|nl|de|fr]+[.]/))) {
            document.cookie = `NEXT_LOCALE=${locale}`
            location.href = originStart + '://' + locale + '.' + originEnd
        }
    }, [])

    return <AppContextWrapper>
        <SWRConfig value={{
            use: [logger],
            shouldRetryOnError: false,
            dedupingInterval: 5000,
        }}>
            <Header asPath={asPath}/>
            <div className="page">
                <div className="page__container">
                    <AsideMenu/>
                    <main className="page__main">
                        <Component {...pageProps} />
                    </main>
                    <AsideBanner/>
                </div>
            </div>
            <Footer/>
        </SWRConfig>
    </AppContextWrapper>
}

export default appWithTranslation(App)