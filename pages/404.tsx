import {useTranslation} from "next-i18next";
import type {NextPage} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

const Error404: NextPage = () => {
    const {t} = useTranslation('common')
    return <h1>{t('404')}</h1>
}

export const getStaticProps = async ({locale}: { locale: string }): Promise<any> => {
    return {
        props: {
            ...await serverSideTranslations(locale, ['404']),
        },
    }
}

export default Error404