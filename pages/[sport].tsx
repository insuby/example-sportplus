import {
    dummyGameWithCoeff,
    dummyGameWithScoreHalf,
    dummyGameWithScoreQuarter,
    dummyGameWithScoreSet
} from "@/types/games";
import Image from "next/image";
import {Fragment} from 'react';
import dynamic from "next/dynamic";
import Tabs from "@/components/Tabs";
import {useTranslation} from "next-i18next";
import img from "@/public/images/1-win.svg";
import {useSportTranslations} from "@/useRequest";
import CallToAction from "@/components/CallToAction";
import SportPageShimmer from "@/pages/[sport]/shimmer";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {GetStaticProps} from "next";
import {useAppContext} from "@/context";

const GameWithCoeff
    = dynamic(() => import("@/components/Game/GameWithCoeff"))
const GameWithScoreSet
    = dynamic(() => import("@/components/Game/GameWithCoeffSet"))
const GameWithScoreHalf
    = dynamic(() => import("@/components/Game/GameWithScoreHalf"))
const GameWithScoreQuarter
    = dynamic(() => import("@/components/Game/GameWithShimmerScoreQuarter"))

const Sport = () => {
    // @ts-ignore
    const {t} = useTranslation('sport-page')
    const {sportContext: {activeSport}}: any = useAppContext()
    const {data, isLoading} = useSportTranslations()

    if (isLoading) return <>
        <Tabs title={t('TITLE', {sport: activeSport.name})}/>
        <SportPageShimmer/>
    </>

    // @ts-ignore
    const {tournaments}: SportProps = data

    return <Fragment>
        <Tabs title={t('TITLE', {sport: activeSport.name})}/>
        {Array.isArray(tournaments) &&
            tournaments.map(({id}) => {
                return <Fragment key={id}>
                    <section className="translation__block">
                        <div className="translation__champ">
                            {/*<Link href={{*/}
                            {/*    pathname: '/[sport]/[tournament]',*/}
                            {/*    query: {*/}
                            {/*        sport: sportURI,*/}
                            {/*        tournament: tournamentURI,*/}
                            {/*    },*/}
                            {/*}} children={name}/>*/}
                            <i className="translation__icon _icon-star-off"/>
                        </div>
                        <GameWithCoeff {...dummyGameWithCoeff}/>
                        <GameWithScoreHalf {...dummyGameWithScoreHalf}/>
                        <GameWithScoreSet {...dummyGameWithScoreSet}/>
                        {/*@ts-ignore*/}
                        <GameWithScoreQuarter {...dummyGameWithScoreQuarter}/>
                    </section>
                    <section className="translation__banner">
                        <a className="translation__banner-link" href="">
                            <div className="translation__banner-img one-win">
                                <Image src={img} alt="img"/>
                            </div>
                            <div className="translation__banner-text">{t('BONUS')}</div>
                        </a>
                    </section>
                </Fragment>
            })}
        <CallToAction/>
    </Fragment>
};

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...await serverSideTranslations(locale as string, ["tabs", "common", "sport-page"]),
        },
    }
}

export async function getStaticPaths() {
    return {
        paths: [{params: {sport: ''}}],
        fallback: true
    };
}

export default Sport;