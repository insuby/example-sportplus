import Translation from "@/components/Translation";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {GetStaticProps} from "next";

const Home = () => <Translation/>

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...await serverSideTranslations(locale as string, ["categories", "tabs", "common", "home-page"]),
        },
    }
}

export default Home
