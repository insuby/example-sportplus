import {Fragment} from 'react';
import Image from "next/image";
import banner from '@/public/images/1-win.svg'
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {NextPage} from "next";
import {useTranslation} from "next-i18next";
import {FAVORITE} from "@/utils/constants";

const Favorite: NextPage = () => {
    const {t} = useTranslation(FAVORITE)
    return <Fragment>
        <h1 className="translation__title title-b">{t('FAVORITE')}</h1>
        <section className="translation__banner">
            <div className="translation__banner-img onex-bet">
                <Image src={banner} alt="banner"/>
            </div>
            <div className="translation__banner-text">Лучшие коэффициенты с 0% маржой на многие ТОП матчи</div>
            {/*</a>*/}
        </section>
        <section className="translation__block">
            <div className="translation__condition _icon-star-off">
                <div className="title">Избранных событий нет</div>
                <div className="grey">Добавляйте турниры и матчи в избранное для быстрого
                    доступа к актуальным для Вас событиям.
                </div>
                {/*<a className="btn" href="">Все трансляции</a>*/}
            </div>
        </section>
    </Fragment>
};

export const getStaticProps = async ({locale}: { locale: string }): Promise<any> => {
    return {
        props: {
            ...await serverSideTranslations(locale, ["categories", "tabs", "common"]),
        },
    }
}

export default Favorite;