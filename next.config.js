const path = require("path");
const {i18n} = require("./next-i18next.config")
const withBundleAnalyzer = require("@next/bundle-analyzer")({
	enabled: process.env["ANALYZE"] === "true111"
});
const DuplicatePackageCheckerPlugin = require("duplicate-package-checker-webpack-plugin")

const nextConfig = {
	trailingSlash: true,
	images: {
		domains: [`${process.env.ASSET_PREFIX}`]
	},
	i18n,
	performance: true,
	webpack5: true,
	reactStrictMode: true,
	optimizeImages: true,
	optimizeCss: true,
	productionBrowserSourceMaps: false,
	poweredByHeader: false,
	sassOptions: {
		// includePaths: [path.join(__dirname, 'styles'), path.join(__dirname, 'components/*')],
		includePaths: [path.join(__dirname, "styles")]
		// prepend:[
		//     path.join(__dirname, 'styles/main/vars.scss'),
		//     path.join(__dirname, 'styles/main/mixins.scss'),
		//     path.join(__dirname, 'styles/main/media.scss'),
		// ],
	},
	webpack: (config, options) => {
		config.plugins.push(
			new DuplicatePackageCheckerPlugin(),
			new options.webpack.ContextReplacementPlugin(/\.\/locale$/, "empty-module", false, /js$/)
		);
		config.resolve.alias["react-is"] = path.resolve(__dirname, "node_modules/react-is")
		return config
	}
}

module.exports = withBundleAnalyzer(nextConfig)