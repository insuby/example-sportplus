import IconFavorite from "@/components/Translation/IconFavorite";

const Champ = ({name, id}: { name: string, id: number }) => {
    return <div className="translation__champ">
        <h3 className="title-sm">{name}</h3>
        <IconFavorite id={id}/>
    </div>

};

export default Champ;