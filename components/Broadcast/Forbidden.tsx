import IconFavorite from "@/components/Translation/IconFavorite";

const Forbidden = ({name, id}: {name: string, id: number}) => {
    return (
        <section className="translation__block">
            <div className="translation__champ">
                <h3 className="title-sm">{name}</h3>
                <IconFavorite id={id}/>
            </div>
            <div className="translation__condition _icon-attention">
                <span className="title">Онлайн трансляция недоступна на территории вашей страны</span>
            </div>
        </section>
    );
};

export default Forbidden;