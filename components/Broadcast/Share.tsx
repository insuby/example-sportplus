const Share = () => {
    return (
        <section className="translation__block">
            <div className="translation__champ">
                <h3 className="title-sm">Поделиться трансляцией</h3>
            </div>
            <div className="translation__social">
                <a className="_icon-fb" href=""/>
                <a className="_icon-vk" href=""/>
                <a className="_icon-ok" href=""/>
                <a className="_icon-tw" href=""/>
                <a className="_icon-tl" href=""/>
                <button className="btn btn--orng _icon-link">Скопировать ссылку</button>
            </div>
        </section>
    );
};

export default Share;