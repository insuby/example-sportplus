const BannerSmallShimmer = () => {
    return (
        <section className="translation__banner ">
            <a className="translation__banner-link">
                <div className="translation__banner-img one-win shimmer"/>
                <div className="translation__banner-text shimmer"/>
            </a>
        </section>
    );
};

export default BannerSmallShimmer;