import Image from 'next/image'
import banner from '@/public/images/banner.png'
import type {FunctionComponent} from 'react';

const BannerBig: FunctionComponent = () => {
    return (
        <aside className="page__aside-banner">
            <a className="aside__inner" href="">
                <Image src={banner} alt="banner"/>
            </a>
        </aside>
    );
};

export default BannerBig;