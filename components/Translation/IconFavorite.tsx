import {useAppContext} from "@/context";
import type {FunctionComponent} from 'react';

const IconFavorite: FunctionComponent<{ id: number }> = ({id}) => {
    // @ts-ignore
    const {favoriteContext: {favorite, setFavorite}} = useAppContext()

    const onClick = () => {
        // @ts-ignore
        let prev = JSON.parse(localStorage.getItem('favorite')) ?? favorite
        prev.includes(id)
            ? prev.splice(prev.indexOf(id), 1)
            : prev.push(id)
        localStorage.setItem('favorite', JSON.stringify(prev))
        // @ts-ignore
        setFavorite(JSON.parse(localStorage.getItem('favorite')))
    }
    return <i onClick={onClick} className={`translation__icon _icon-star-${!!favorite.includes(id) ? 'on' : 'off'}`}/>
};

export default IconFavorite;