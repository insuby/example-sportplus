const GameShimmerCoeff = () => <section className="translation__block shimmer__box sb">
    <div className="translation__champ">
        <a href="/" className={'shimmer'}>content</a>
        <i className="translation__icon _icon-star-off shimmer"/>
    </div>
    <div className="translation__match">
        <a className="translation__inner" href="">
            <div className="translation__column">
                <span className={'shimmer'}>15:00</span>
                <span className="shimmer">25.08</span>
            </div>
            <div className="translation__column">
                                <span className={'shimmer'}>
                                    <span>Челси</span>
                                </span>
                <span className={'shimmer'}>
                                    <span>Эвертон</span>
                                </span>
            </div>
            <div className="translation__column">
                <div className="translation__scores shimmer">
                    <span/>
                    <span>3.05</span>
                </div>
                <div className="translation__scores shimmer">
                    <span>/</span>
                    <span>3.05</span>
                </div>
                <div className="translation__scores shimmer">
                    <span/>
                    <span>3.05</span>
                </div>
            </div>
        </a>
        <i className="translation__icon _icon-star-off shimmer"/>
    </div>
</section>

export default GameShimmerCoeff