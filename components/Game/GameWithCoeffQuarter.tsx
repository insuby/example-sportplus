import Image from "next/image";
import type {FunctionComponent} from 'react';
import type {GameWithScoreQuarterType} from "@/types/games";
import IconFavorite from "@/components/Translation/IconFavorite";
import LinkWrapper from "HOC/LinkWrapper";

const GameWithScoreQuarter: FunctionComponent<GameWithScoreQuarterType> =
    ({
         id,
         href,
         images,
         participants,
         periodPostfix,
         score,
         currentTime,
         periods,
     }) => {
        return (
            <div className="translation__match">
                <LinkWrapper href={href}>
                    <a className="translation__inner">
                        <div className="translation__column">
                            <span className="orange">{currentTime}'</span>
                            <span className="orange">{periodPostfix}</span>
                        </div>
                        <div className="translation__column">
                             <span>
                                <Image width={18} height={18} src={images[0]} alt="image"/>
                                <span>{participants[0].name}</span>
                            </span>
                            <span>
                                <Image width={18} height={18} src={images[1]} alt="image"/>
                                <span>{participants[1].name}</span>
                             </span>
                        </div>
                        <div className="translation__column">
                            <div className="translation__score">
                                <span>{score[0]}</span>
                                <span>{score[1]}</span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">{periods[0]}</span>
                                <span className="grey">{periods[1]}</span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">{periods[2]}</span>
                                <span className="grey">{periods[3]}</span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">{periods[4]}</span>
                                <span className="grey">{periods[5]}</span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">{periods[6]}</span>
                                <span className="grey">{periods[7]}</span>
                            </div>
                        </div>
                    </a>
                </LinkWrapper>
                <IconFavorite id={id}/>
            </div>
        );
    };

export default GameWithScoreQuarter;