import Link from 'next/link';
import type {LinksGroupProps} from "@/types/props";

const LinksGroup = ({group}: LinksGroupProps) => {
    return group.map(({id, name, sportURI}) => {
        return <li key={id} className="footer__item">
            <Link href={{
                pathname: `/[sport]`,
                query: {sport: sportURI, name, id},
            }}>
                <a className="footer__link">{name}</a>
            </Link>
        </li>
    })
};

export default LinksGroup;