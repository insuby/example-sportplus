import Link from "next/link";
import Image from 'next/image'
import {useTranslation} from 'next-i18next';
import {PAGES_ROUTES} from "@/utils/constants";
import {useTranslationsGroups} from "@/useRequest";
import logoGrey from '@/public/images/logo-grey.svg';
import LinksGroup from "@/components/Footer/LinksGroup";
import type {FunctionComponent} from "react";

const Footer: FunctionComponent = () => {
    const {t} = useTranslation(['categories', 'common'])
    const groups = useTranslationsGroups()

    return <footer className={'footer'}>
        <div className={'footer__container'}>

            <div className="footer__about">
                <Link href={PAGES_ROUTES['HOME'].route}>
                    <a className="footer__logo">
                        <Image src={logoGrey} alt="logo" width="155"/>
                    </a>
                </Link>
                <div className="footer__text text-sm">Спорт Плюс - бесплатный сервис для просмотра спортивных трансляций
                    в прямом эфире.
                </div>
            </div>

            <div className="footer__translation">
                <div className="footer__title title-sm">{t('common:TRANSLATIONS')}</div>
                <nav className="footer__body">
                    <ul className="footer__list">
                        {groups.map(group => {
                            // @ts-ignore
                            return <LinksGroup key={group[0].sportURI} group={group}/>
                        })}
                    </ul>
                </nav>
            </div>
            <div className="footer__more">
                <div className="footer__title title-sm">{t('common:ADDITIONAL')}</div>
                <nav className="footer__body">
                    <ul className="footer__list">
                        <li className="footer__item">
                            <Link href={PAGES_ROUTES['BOOKMAKERS'].route}>
                                <a className="footer__link">{t('common:BOOKMAKERS')}</a>
                            </Link>
                        </li>
                        <li className="footer__item">
                            <Link href={PAGES_ROUTES['SUPPORT'].route}>
                                <a className="footer__link">{t('common:SUPPORT')}</a>
                            </Link>
                        </li>
                        <li className="footer__item">
                            <Link href={PAGES_ROUTES['BONUSES'].route}>
                                <a className="footer__link">{t('common:BONUSES')}</a>
                            </Link>
                        </li>
                        <li className="footer__item">
                            <Link href={PAGES_ROUTES['COPYRIGHT'].route}>
                                <a className="footer__link">{t('common:COPYRIGHT')}</a>
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
}

export default Footer