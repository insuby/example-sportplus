import type {MouseEventHandler} from "react";

const Button = ({onClick, className, text}: {
    onClick: MouseEventHandler<HTMLButtonElement>,
    className: string,
    text: string
}) => {
    return <button
        type="button"
        onClick={onClick}
        className={className}
        children={text}
    />
};

export default Button;