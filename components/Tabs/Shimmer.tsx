import type {FunctionComponent} from "react";

const TabsShimmer: FunctionComponent = (): JSX.Element => {
    return <div className={"translation__tabs tabs"}>
        <button className={'tabs__item text btn shimmer'}/>
        <button className={'tabs__item text btn shimmer'}/>
        <button className={'tabs__item text btn shimmer'}/>
    </div>
};

export default TabsShimmer;