import LinkWrapper from "HOC/LinkWrapper";

const Breadcrumbs = () => {
    const links = [
        {name: 'Футбол', href: 'footbal'},
        {name: 'Чемпионат Англии. Премьер-лига', href: '12312'},
    ]
    return (
        <div className="bread-crumbs">
            <ul className="bread-crumbs__list">
                {links.map(({href, name}) => {
                    return <LinkWrapper href={href}>
                        <li className="bread-crumbs__item">
                            <a className="bread-crumbs__link">{name}</a>
                        </li>
                    </LinkWrapper>
                })}
            </ul>
        </div>
    );
};

export default Breadcrumbs;