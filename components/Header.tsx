import Image from 'next/image'
import {useEffect, useState} from "react";
import logo from '@/public/images/logo.svg';
import {useTranslation} from "next-i18next";
import {ACTIVE, ASIDE_OPEN, PAGES_ROUTES} from "@/utils/constants";
import type {HeaderComponent} from "@/types/components";
import LinkWrapper from "HOC/LinkWrapper";

const Header: HeaderComponent = ({asPath}) => {
    const {t} = useTranslation('common')
    const [open, setOpen] = useState<boolean>(false)

    useEffect(() => {
        document.body.classList.remove(ASIDE_OPEN)
        open && document.body.classList.add(ASIDE_OPEN)
    }, [open])

    return <header className="header">
        <div className="header__container">
            <LinkWrapper href={PAGES_ROUTES['HOME'].route}>
                <a className="header__logo">
                    <Image src={logo} alt="logo" width="155"/>
                </a>
            </LinkWrapper>
            <nav className="header__nav">
                <div className="header__brgr" onClick={() => setOpen(!open)}>
                    <span/>
                    <span/>
                    <span/>
                </div>
                <ul className="header__list">
                    {[
                        PAGES_ROUTES['HOME'],
                        PAGES_ROUTES['BOOKMAKERS'],
                        PAGES_ROUTES['BONUSES'],
                    ].map(({name, route}) => {
                        return <li
                            key={name}
                            className={`header__item ${asPath === route ? ACTIVE : ''}`}
                        >
                            <LinkWrapper href={route}>
                                <a className="header__link">{t(name)}</a>
                            </LinkWrapper>
                        </li>
                    })}
                </ul>
            </nav>
        </div>
    </header>
};

export default Header;