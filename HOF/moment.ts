import moment from "moment";

import 'moment/locale/ru';
import 'moment/locale/de';
import 'moment/locale/nl';
import 'moment/locale/fr';

((moment) => {
    moment.defaultFormat = 'LT'
    // @ts-ignore
    moment.countdown = (eventTime) => {
        const diff = eventTime - Date.now()
        const duration = moment.duration(diff * 1000, 'milliseconds')

        return {
            days: duration.days(),
            hours: duration.hours(),
            minutes: duration.minutes(),
        }
    }

    return moment
})(moment)


export default moment