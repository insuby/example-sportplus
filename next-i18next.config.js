const origin = process.env.mode === "production"
	? process.env.APP_PRODUCTION_ORIGIN
	: process.env.APP_DEVELOPMENT_ORIGIN

const locales = process.env.NEXT_PUBLIC_LOCALES_LIST.split(",");

const domains = locales.map(locale => {
	return {
		domain: `${locale}.${origin}`,
		defaultLocale: `${locale}`,
		locales: [locale]
	}
})

module.exports = {
	i18n: {
		defaultLocale: "ru",
		locales,
		domains,
		localeDetection: false,
		react: {useSuspense: false}
	}
}
